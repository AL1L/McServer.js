import net from 'net';
import HandshakeSchema from '../packets/schemas/handshake-schema';
import Packet from '../packets/packet';
import PacketString from '../packets/data-types/packet-string';
import PacketInt from '../packets/data-types/packet-int';
import PacketPosition from '../packets/data-types/packet-position';
import PacketDouble from '../packets/data-types/packet-double';
import PacketFloat from '../packets/data-types/packet-float';
import PacketVarInt from '../packets/data-types/packet-var-int';
import ChunkUtil from './chunk-util';
import { EventEmitter } from 'events';

export default class Client extends EventEmitter {
    /**
     * 
     * @param {net.Socket} socket
     * @param {HandshakeSchema} hs
     * @param {Packet} packet
     */
    constructor(socket, hs, packet) {
        super();

        this.state = 0;
        this.key = `${socket.remoteFamily}#${socket.remoteAddress}:${socket.remotePort}`;
        this.username = `AL_1`;

        console.log('New connection', this.key);

        var uuid = new PacketString('268c5cf2-3f3f-4aa3-bbd9-3fade63fb658').toBuffer();
        var user = new PacketString(this.username).toBuffer();
        socket.write(new Packet(0x02, Buffer.concat([uuid, user])).toBuffer());

        this.state = 1;

        var joinArr = [
            new PacketInt(0).toBuffer(),
            Buffer.from([0x01]),
            new PacketInt(0).toBuffer(),
            Buffer.from([0x00]),
            Buffer.from([0x00]),
            new PacketString("flat").toBuffer(),
            Buffer.from([0x00])
        ];
        socket.write(new Packet(0x23, Buffer.concat(joinArr)).toBuffer());

        var pos = new PacketPosition(0, 1, 0);
        socket.write(new Packet(0x46, pos).toBuffer());

        var look = Buffer.concat([
            new PacketDouble(0).toBuffer(),
            new PacketDouble(1).toBuffer(),
            new PacketDouble(0).toBuffer(),
            new PacketFloat(0).toBuffer(),
            new PacketFloat(0).toBuffer(),
            Buffer.from([0x00]),
            new PacketVarInt(0).toBuffer()
        ]);
        socket.write(new Packet(0x2F, look).toBuffer());

        this.state = 2;

        socket.on("data", data => {
            var inPacket = Packet.from(data);
            console.log(`New packet 0x${inPacket.packetId.toString(16)}`, this.key)

            var outPacket;
            switch (inPacket.packetId) {
                case 0x00:
                    if (this.state === 0) {
                        this.username = PacketString.from(inPacket.packetData).value;
                        console.log(`Logging in ${this.username}`);
                    } else if (this.state === 2) {
                        var id = PacketVarInt.from(inPacket.packetData).value;
                        if (id === 0) {
                        }
                    }
                    break;
                case 0x03:
                case 0x04:
                    if (this.state !== 2)
                        return;
                    console.log('chunks');
                    // Chunk stuff
                    socket.write(new Packet(0x20, ChunkUtil.buildChunk(0, 0)).toBuffer());

                    // for (let x = 0; x <= 7; x++) {
                    //     for (let y = 0; y <= 7; y++) {
                    //         socket.write(new Packet(0x20, ChunkUtil.buildChunk(x, y)).toBuffer());
                    //     }
                    // }
                    // for (let x = -7; x < 0; x++) {
                    //     for (let y = -7; y < 0; y++) {
                    //         socket.write(new Packet(0x20, ChunkUtil.buildChunk(x, y)).toBuffer());
                    //     }
                    // }


                    socket.write(new Packet(0x2F, look).toBuffer());
                    break;
                default:
                    break;
            }

            if (outPacket != null) {
                socket.write(outPacket.toBuffer());
            }
        });
    }
}
