import PacketInt from "../packets/data-types/packet-int";
import PacketBoolean from "../packets/data-types/packet-boolean";
import PacketVarInt from "../packets/data-types/packet-var-int";
import PacketLong from "../packets/data-types/packet-long";


const CHUNK_HEIGHT = 256;
const SECTION_HEIGHT = 16;
const SECTION_WIDTH = 16;
const BITS_PER_BLOCK = 13;

export default class ChunkUtil {
    /**
     * 
     * @param {number} x 
     * @param {number} z 
     * @returns {Buffer}
     */
    static buildChunk(x, z) {
        var data = [];

        // Chunk meta
        data.push(new PacketInt(x).toBuffer());
        data.push(new PacketInt(z).toBuffer());
        data.push(new PacketBoolean(true).toBuffer());

        // Chunk sections
        var mask = 0;
        var columnBuffer = [];
        for (var sectionY = 0; sectionY < (CHUNK_HEIGHT / SECTION_HEIGHT); sectionY++) {
            if (sectionY !== 0) {
                mask |= (1 << sectionY);  // Set that bit to true in the mask
                ChunkUtil.buildChunkSection(x, sectionY, z, columnBuffer);
            }
        }

        // Biome
        for (var bz = 0; bz < SECTION_WIDTH; bz++) {
            for (var bx = 0; bx < SECTION_WIDTH; bx++) {
                data.push(Buffer.from([127]));
            }
        }

        // Putting data
        data.push(new PacketVarInt(mask).toBuffer());

        columnBuffer = Buffer.concat(columnBuffer)
        data.push(new PacketVarInt(columnBuffer.length).toBuffer());
        data.push(columnBuffer);

        // Tile/Block entities
        data.push(new PacketVarInt(0).toBuffer());

        // Return
        return Buffer.concat(data);
    }
    /**
     * 
     * @param {number} x 
     * @param {number} y
     * @param {number} z
     * @param {Array<Buffer>} buf
     * @returns {Buffer}
     */
    static buildChunkSection(cx, cy, cz, buf) {
        // Palette palette = section.palette;
        var bitsPerBlock = BITS_PER_BLOCK;

        buf.push(Buffer.from([bitsPerBlock]));
        buf.push(new PacketVarInt(0).toBuffer());
        // palette.Write(buf);

        var dataLength = (16 * 16 * 16) * bitsPerBlock / 64; // See tips section for an explanation of this calculation
        var data = Buffer.alloc(dataLength);

        // A bitmask that contains bitsPerBlock set bits
        var individualValueMask = ((1 << bitsPerBlock) - 1);

        for (var y = 0; y < SECTION_HEIGHT; y++) {
            for (var z = 0; z < SECTION_WIDTH; z++) {
                for (var x = 0; x < SECTION_WIDTH; x++) {
                    var blockNumber = (((y * SECTION_HEIGHT) + z) * SECTION_WIDTH) + x;
                    var startLong = (blockNumber * bitsPerBlock) / 64;
                    var startOffset = (blockNumber * bitsPerBlock) % 64;
                    var endLong = ((blockNumber + 1) * bitsPerBlock - 1) / 64;

                    var state = 7;

                    var value = 7;

                    if(y !== 0) {
                        state = 0;
                        value = 0;
                    }

                    value &= individualValueMask;

                    data[startLong] |= (value << startOffset);

                    if (startLong != endLong) {
                        data[endLong] = (value >> (64 - startOffset));
                    }
                }
            }
        }

        buf.push(new PacketVarInt(dataLength).toBuffer());
        data.forEach(d => {
            buf.push(new PacketLong(d).toBuffer());
        })

        for (var y = 0; y < SECTION_HEIGHT; y++) {
            for (var z = 0; z < SECTION_WIDTH; z++) {
                for (var x = 0; x < SECTION_WIDTH; x += 2) {
                    // Note: x += 2 above; we read 2 values along x each time
                    var value = 15 | (15 << 4);
                    buf.push(Buffer.from([value]));
                }
            }
        }

        if (true) { // IE, current dimension is overworld / 0
            for (var y = 0; y < SECTION_HEIGHT; y++) {
                for (var z = 0; z < SECTION_WIDTH; z++) {
                    for (var x = 0; x < SECTION_WIDTH; x += 2) {
                        // Note: x += 2 above; we read 2 values along x each time
                        var value = 15 | (15 << 4);
                        buf.push(Buffer.from([value]));
                    }
                }
            }
        }
    }
}
