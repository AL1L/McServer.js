import PacketVarInt from './data-types/packet-var-int';
import PacketType from './data-types/packet-type';

export default class Packet {

    /**
     * 
     * @param {number} packetId 
     * @param {any|PacketType} packetData 
     * @param {string} encoding 
     */
    constructor(packetId, packetData, encoding = 'utf8') {
        this.encoding = encoding;
        this.packetId = packetId;
        this.packetData = packetData;
    }

    set packetData(packetData) {
        /**
         * @type {Buffer}
         */
        this.rawPacketData = new Buffer(0);
        this._packetData = packetData;

        if (packetData instanceof Buffer) {
            this.rawPacketData = packetData;
        } else if (packetData instanceof PacketType) {
            this.rawPacketData = packetData.toBuffer();
        } else if (typeof packetData === 'string') {
            this.rawPacketData = Buffer.concat([Packet.numberToVarInt(packetData.length), Buffer.from(packetData, this.encoding)]);
        } else if (typeof packetData === 'object') {
            if (packetData instanceof Array) {
                this.rawPacketData = Buffer.from(packetData);
            } else {
                var str = JSON.stringify(packetData);
                this.rawPacketData = Buffer.concat([Packet.numberToVarInt(str.length), Buffer.from(str, 'utf8')]);
            }
        } else if (typeof packetData === 'number') {
            if (this.encoding === 'long') {
                this.rawPacketData = Packet.longToByteArray(packetData);
            } else {

            }
        }
    }

    set packetId(id) {
        /**
         * @type {Buffer}
         */
        var type = new PacketVarInt(id);
        this.rawPacketId = type.toBuffer();
        this._packetId = id;
    }

    get packetId() {
        return this._packetId;
    }

    get packetData() {
        return this._packetData;
    }

    get packetLength() {
        return this.rawPacketData.length + this.rawPacketId.length;
    }

    get rawPacketLength() {
        return Packet.numberToVarInt(this.packetLength);
    }

    toBuffer() {
        return Buffer.concat([this.rawPacketLength, this.rawPacketId, this.rawPacketData]);
    }

    toString() {
        return `0x${this.packetId}-${JSON.stringify(this.rawPacketData.toJSON().data)}`;
    }

    /**
     * 
     * @param {number} value 
     * @returns {Buffer}
     */
    static numberToVarInt(value) {
        var bytes = [];
        do {
            var temp = (value & 0b01111111);
            value >>>= 7;
            if (value != 0) {
                temp |= 0b10000000;
            }
            bytes.push(temp)
        } while (value != 0);
        return Buffer.from(bytes);
    }

    /**
     * 
     * @param {Buffer|Array<number>|Uint8Array} data The data to decode
     * @param {number} start offset
     */
    static readVarInt(data, start = 0) {
        var numRead = 0;
        var result = 0;
        var read;
        do {
            read = data[numRead + start];
            var value = (read & 0b01111111);
            result |= (value << (7 * numRead));

            numRead++;
            if (numRead > 5) {
                console.error("VarInt is too big");
                break;
            }
        } while ((read & 0b10000000) != 0);
        return {
            result: result,
            length: numRead
        };
    }

    static longToByteArray(long) {
        // we want to represent the input as a 8-bytes array
        var byteArray = [0, 0, 0, 0, 0, 0, 0, 0];

        for (var index = 0; index < byteArray.length; index++) {
            var byte = long & 0xff;
            byteArray[index] = byte;
            long = (long - byte) / 256;
        }
        byteArray.reverse();
        return Buffer.from(byteArray);
    }

    /**
     * 
     * @param {Buffer} buffer 
     */
    static from(buffer) {
        var length = Packet.readVarInt(buffer);
        var packetID = Packet.readVarInt(buffer, length.length);
        var packetData = buffer.slice(length.length + packetID.length, buffer.length);

        return new Packet(packetID.result, packetData);
    }
}
