import PacketType from "./packet-type";
import PacketVarInt from "./packet-var-int";

export default class PacketPosition extends PacketType {
    constructor(x, y, z) {
        super();

        this.value = [x, y, z];
    }

    toByteArray(value) {
        const x = value[0];
        const y = value[1];
        const z = value[2];

        value = ((x & 0x3FFFFFF) << 38) | ((y & 0xFFF) << 26) | (z & 0x3FFFFFF);

        var byteArray = [0, 0, 0, 0, 0, 0, 0, 0];

        for (var index = 0; index < byteArray.length; index++) {
            var byte = value & 0xff;
            byteArray[index] = byte;
            value = (value - byte) / 256;
        }
        byteArray.reverse();
        return Buffer.from(byteArray);
    }

    static from(buffer, offset = 0) {

    }
}
