import PacketType from "./packet-type";
import PacketLong from "./packet-long";

export default class PacketDouble extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }

    toByteArray(num) {
        var view = new DataView(new ArrayBuffer(8));
        view.setFloat64(0, num);
        return new PacketLong(view.getInt32(0)).toBuffer()
    }

    static from(buffer, offset = 0) {
        
    }
}
