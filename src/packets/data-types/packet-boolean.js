import PacketType from "./packet-type";

export default class PacketBoolean extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }
    
    toByteArray(value) {
        return Buffer.from([value ? 0x01 : 0x00]);
    }

    static from(buffer, offset = 0) {
        return new PacketBoolean(buffer[offset] === 0x01);
    }
}
