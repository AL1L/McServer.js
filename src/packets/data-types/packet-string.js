import PacketType from "./packet-type";
import PacketVarInt from "./packet-var-int";

export default class PacketString extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }

    toByteArray(value) {
        if(typeof value === 'object') {
            value = JSON.stringify(value);
        } else if(value == null) {
            value = "null";
        }
        return Buffer.concat([new PacketVarInt(value.length).toBuffer(), Buffer.from(value)]);
    }

    static from(buffer, offset = 0) {
        var b2 = Buffer.from(buffer);
        var length = PacketVarInt.from(buffer, offset);
        var string = b2.slice(offset + length.bufferLength, length.value + offset + 1).toString();

        return new PacketString(string);
    }
}
