import PacketType from "./packet-type";

export default class PacketShort extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }

    toByteArray(value) {
        var byteArray = [0, 0];

        for (var index = 0; index < byteArray.length; index++) {
            var byte = value & 0xff;
            byteArray[index] = byte;
            value = (value - byte) / 256;
        }
        byteArray.reverse();
        return Buffer.from(byteArray);
    }

    static from(buffer, offset = 0) {
        var value = 0;
        for (var i = 1 + offset; i >= 0 + offset; i--) {
            value = (value * 256) + buffer[i];
        }

        return new PacketShort(value);
    }
}
