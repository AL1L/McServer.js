import PacketType from "./packet-type";

export default class PacketByte extends PacketType {
    constructor(value) {
        super();

        value = parseInt(value);
        if (value === NaN) {
            value = 0;
        }
        this.value = value;
    }

    toByteArray(value) {
        return Buffer.from([value + 128]);
    }

    static from(buffer, offset = 0) {
        return new PacketByte(buffer[offset] - 128);
    }
}
