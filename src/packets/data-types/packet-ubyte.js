import PacketType from "./packet-type";

export default class PacketUByte extends PacketType {
    constructor(value) {
        super();

        value = parseInt(value);
        if (value === NaN) {
            value = 0;
        }
        this.value = value;
    }

    toByteArray(value) {
        return Buffer.from([value]);
    }

    static from(buffer, offset = 0) {
        return new PacketUByte(buffer[offset]);
    }
}
