
export default class PacketType {

    constructor() { 
        this.rawValue = new Buffer(0);
    }

    set value(value) {
        /**
         * @private
         */
        this._value = value;
        /**
         * @type {Buffer}
         */
        this.rawValue = this.toByteArray(value);
    }

    get value() {
        return this._value;
    }

    toBuffer() {
        return this.rawValue;
    }

    get bufferLength() {
        return this.rawValue.length;
    }
}
