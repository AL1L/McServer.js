import PacketType from "./packet-type";

export default class PacketVarLong extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }

    toByteArray(value) {
        if (this.value < -2147483648)
            this.value = -2147483648;
        if (this.value > 2147483647)
            this.value = 2147483647;
        var bytes = [];
        do {
            var temp = (value & 0b01111111);
            value >>>= 7;
            if (value != 0) {
                temp |= 0b10000000;
            }
            bytes.push(temp);
        } while (value != 0);
        return Buffer.from(bytes);
    }

    static from(buffer, offset = 0) {
        var numRead = 0;
        var result = 0;
        var read;
        do {
            read = buffer[numRead + offset];
            var value = (read & 0b01111111);
            result |= (value << (7 * numRead));

            numRead++;
            if (numRead > 10) {
                console.error("VarLong is too big");
                break;
            }
        } while ((read & 0b10000000) != 0);
        return new PacketVarLong(result);
    }
}
