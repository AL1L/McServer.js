import PacketType from "./packet-type";
import PacketInt from "./packet-int";

export default class PacketFloat extends PacketType {
    constructor(value) {
        super();

        this.value = value;
    }

    toByteArray(num) {
        var view = new DataView(new ArrayBuffer(4));
        view.setFloat32(0, num);
        return new PacketInt(view.getInt32(0)).toBuffer()
    }

    static from(buffer, offset = 0) {
        var binary = parseInt(buffer.toString('hex'), 16).toString(2);
        if (binary.length < 32)
            binary = ('00000000000000000000000000000000' + binary).substr(binary.length);
        var sign = (binary.charAt(0) == '1') ? -1 : 1;
        var exponent = parseInt(binary.substr(1, 8), 2) - 127;
        var significandBase = binary.substr(9);
        var significandBin = '1' + significandBase;
        var i = 0;
        var val = 1;
        var significand = 0;

        if (exponent == -127) {
            if (significandBase.indexOf('1') == -1)
                return 0;
            else {
                exponent = -126;
                significandBin = '0' + significandBase;
            }
        }

        while (i < significandBin.length) {
            significand += val * parseInt(significandBin.charAt(i));
            val = val / 2;
            i++;
        }

        return sign * significand * Math.pow(2, exponent);
    }
}
