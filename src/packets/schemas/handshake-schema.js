import PacketVarInt from "../data-types/packet-var-int";
import PacketString from "../data-types/packet-string";
import PacketShort from "../data-types/packet-short";

export default class HandshakeSchema {

    constructor(buffer) {
        var offset = 0;

        this.version = PacketVarInt.from(buffer, offset);
        offset += this.version.bufferLength

        this.serverAddress = PacketString.from(buffer, offset);
        offset += this.serverAddress.bufferLength + 2;

        this.status = PacketVarInt.from(buffer, offset);
        offset += this.status.bufferLength;


        this.name = PacketString.from(buffer, offset);

    }
}
