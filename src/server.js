import net from 'net';
import { EventEmitter } from 'events';
import Packet from './packets/packet';
import fs from 'fs';
import PacketType from './packets/data-types/packet-type';
import PacketLong from './packets/data-types/packet-long';
import PacketString from './packets/data-types/packet-string';
import HandshakeSchema from './packets/schemas/handshake-schema';
import Client from './client/client';

var slp = {
    version: {
        name: "1.12.2",
        protocol: 340
    },
    players: {
        max: 100,
        online: 1,
        sample: [
            {
                name: "AL_1",
                id: "268c5cf2-3f3f-4aa3-bbd9-3fade63fb658"
            }
        ]
    },
    description: {
        text: "I WILL WIN THIS!",
        color: "gold"
    },
    favicon: ""
};

export default class Server extends EventEmitter {

    /**
     * 
     * @param {object} settings 
     */
    constructor(settingsFile) {
        super();

        this.clients = {};

        console.log('Loading server...');

        this.settings = JSON.parse(fs.readFileSync(settingsFile));
        /**
         * @type {net.Server}
         */
        this.tcp = net.createServer(socket => {
            this.socketStart(socket);
        }).on("error", err => {
            throw err;
        });

        this.tcp.listen({
            port: 25565,
            host: 'localhost',
            exclusive: true
        }, () => {
            console.log(`Listening on ${this.tcp.address().address}:${this.tcp.address().port}`);
        });
    }

    /**
     * 
     * @param {net.Socket} socket 
     */
    socketStart(socket) {
        const key = `${socket.remoteFamily}#${socket.remoteAddress}:${socket.remotePort}`;
        /**
         * @event Server#tcpOpen
         * @type {net.Server}
         */
        this.emit('tcpOpen', socket);

        socket.on("error", e => {
            console.error(e);
        });
        socket.on("close", (err) => {
            delete this.clients[key];
        });
        socket.on("data", data => {
            if (key in this.clients)
                return;
                
            var inPacket = Packet.from(data);
            // console.log(`New packet 0x${inPacket.packetId.toString(16)}`, key)
            // console.log(inPacket.packetData);
            var outPacket;
            switch (inPacket.packetId) {
                case 0x00:
                    var hs = new HandshakeSchema(inPacket.rawPacketData);
                    if (hs.status.value === 1) {
                        console.log('Ping', socket.remoteAddress);
                        outPacket = new Packet(0x00, new PacketString({
                            version: {
                                name: "1.12.2",
                                protocol: 340
                            },
                            players: {
                                max: this.settings.max_players,
                                online: 0,
                                sample: []
                            },
                            description: this.settings.motd,
                            favicon: ""
                        }));
                    } else {
                        this.clients[key] = new Client(socket, hs, inPacket);
                    }
                    break;
                case 0x01:
                    outPacket = new Packet(0x01, new PacketLong(new Date().getTime()));
                    break;
                default:
                    break;
            }

            if (outPacket != null) {
                socket.write(outPacket.toBuffer());
            }
        });
    }
}
